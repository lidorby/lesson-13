#include "Client.h"
#include<string>
Client::Client(std::string cName, SOCKET& socket)
{
	this->Name = cName;
	this->clientSocket = socket;
}

Client::~Client()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(clientSocket);
	}
	catch (...)
	{
		//do nothing
	}
}
