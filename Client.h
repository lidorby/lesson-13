#pragma once
#include <WinSock2.h>
#include <iostream>

class Client
{

public:
	Client(std::string clientName, SOCKET& socket);
	~Client();

	std::string Name;
	SOCKET clientSocket;
};
