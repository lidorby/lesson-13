#include "Server.h"
#include <mutex>
mutex L1;//to Open messages
mutex L2;//to users
condition_variable conMsg;


void Server::UpdateAllUsers()
{
	Helper help;
	int s = this->_users.size();
	fstream f;
	f.open(this->_dataFile, fstream::in);
	f.seekg(0, f.end);
	int length = f.tellg();
	f.seekg(0, f.beg);

	char * buffer = new char[length + 1]{ 0 };

	f.read(buffer, length);
	f.close();

	queue<Client> tempQ;

	if (s == 1)
	{
		help.sendUpdateMessageToClient(this->_users.front().clientSocket, buffer, this->_users.front().Name, "", 1);
	}
	else
	{
		Client c1 = this->_users.front();
		this->_users.pop();
		Client c2 = this->_users.front();

		pushCFront(c1);

		for (int i = 1; i <= s; i++)
		{
			tempQ.push(this->_users.front());
			Client temp = this->_users.front();

			help.sendUpdateMessageToClient(temp.clientSocket, buffer, c1.Name, c2.Name, i);

			this->_users.pop();
		}

		while (!tempQ.empty())
		{
			this->_users.push(tempQ.front());
			tempQ.pop();
		}

	}
}
void Server::HandleIncome()
{
	while (true)
	{
		unique_lock<mutex> l(L1);
		conMsg.wait(l, [=]() {return !this->msgs.empty(); });
		string msg = this->msgs.front();
		this->msgs.pop();
		l.unlock();

		string type = msg.substr(0, 3);
		L2.lock();
		string curr = this->_users.front().Name;
		L2.unlock();
		if (type == "204")
		{
			this->UpdateFile(msg);
			this->UpdateAllUsers();
		}
		else if (type == "207")
		{
			this->UpdateFile(msg);
			L2.lock();
			Client u = this->_users.front();
			this->_users.pop();
			this->_users.push(u);
			L2.unlock();
			this->UpdateAllUsers();
		}
		else if (type == "208")
		{
			this->_users.pop();
			this->UpdateAllUsers();
		}
		cout << endl << endl;
	}

}

void Server::CatchMsgs(SOCKET clnt)
{
	while (true)
	{
		string msg;
		int type = 0;
		try
		{
			type = Helper::getMessageTypeCode(clnt);
			int size = 0;
			if (type == MT_CLIENT_LOG_IN)
			{
				size = Helper::getIntPartFromSocket(clnt, 2);
				msg = to_string(type) + Helper::getPaddedNumber(size, 2) + Helper::getStringPartFromSocket(clnt, size);
				L2.lock();
				this->_users.push(Client(msg.substr(5), clnt));
				L2.unlock();
				this->UpdateAllUsers();
				break;
			}
			else if (type == MT_CLIENT_FINISH)
			{

				size = Helper::getIntPartFromSocket(clnt, 5);
				msg = to_string(type) + Helper::getPaddedNumber(size, 5) + Helper::getStringPartFromSocket(clnt, size);
				break;
			}
			else if(type== MT_CLIENT_EXIT)
			{
				msg = to_string(type);
				break;
			}
		}
		catch (exception)
		{
			closesocket(clnt);
			break;
		}
		if (msg != "")
		{
			if (type != MT_CLIENT_LOG_IN)
			{
				unique_lock<mutex> l(L1);
				this->msgs.push(msg);
				l.unlock();
				conMsg.notify_all();
			}
		}
	}

}



void Server::UpdateFile(string msg)
{
	ofstream file(_dataFile);

	if (file.is_open())
	{
		file << msg;
		file.close();
	}
	else cout << "cant open file";
}

Server::Server(string dataFile)
{
	serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__  " - socket");
	}

	this->_dataFile = dataFile;
}
Server::~Server()
{
	try
	{
		closesocket(serverSocket);
	}
	catch (...) {}
}

void Server::Listen()
{
	struct sockaddr_in sa = { 0 };
	int port = 7214;

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr("127.0.0.1");   
	if (::bind(this->serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		cout << "Error: " << err << endl;
		throw std::exception("bind error");
	}

	// Start listening
	if (listen(this->serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		cout << "Error: " << err << endl;
		throw std::exception("listen error");
	}
	cout << "Listening on port " << port << endl;

	thread t(&Server::HandleIncome, this);
	t.detach();
	while (1)
	{
		SOCKET connected = ::accept(this->serverSocket, NULL, NULL);

		thread clnt = thread(&Server::CatchMsgs, this, connected);
		clnt.detach();
	}
}


void Server::pushCFront(Client client)
{
	queue<Client> temp;

	while (!this->_users.empty())
	{
		temp.push(this->_users.front());
		this->_users.pop();
	}

	this->_users.push(client);

	while (!temp.empty())
	{
		this->_users.push(temp.front());
		temp.pop();
	}
}