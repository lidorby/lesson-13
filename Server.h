#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <Windows.h>
#include <queue>
#include <iostream>
#include <string>
#include <fstream>
#include "Helper.h"
#include "Client.h"
#include <thread>

using namespace std;

class Server
{
private:
	SOCKET serverSocket;
	queue<string> msgs;
	queue<Client> _users;
	string _dataFile;

	void UpdateAllUsers();
	void HandleIncome();
	void CatchMsgs(SOCKET clnt);
	void UpdateFile(string msg);
	void pushCFront(Client client);
public:
	Server(string dataFile);
	~Server();
	void Listen();

};
